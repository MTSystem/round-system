package net.uveSoftware.MTSystem.RoundSystem;

import lombok.Data;

/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */

@Data
public class RoundLocationInfo {
    private String id;
    private String name;
    private String description;

    public RoundLocationInfo(String id) {
        this.id = id;
        this.name = id;
    }
}
