package net.uveSoftware.MTSystem.WebController;

import lombok.Data;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
@Data
public class PageInfo {
    private int offset;
    private int limit;
    private int totalRegisters;

    public PageInfo(int limit, int offset, int totalRegisters) {
        this.limit = limit;
        this.offset = offset;
        this.totalRegisters = totalRegisters;
    }
}
