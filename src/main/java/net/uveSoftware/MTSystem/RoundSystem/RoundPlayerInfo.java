package net.uveSoftware.MTSystem.RoundSystem;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
public class RoundPlayerInfo {

    private UUID id;
    private String name;
    private String faction;

    private int firstValue;
    private int secondValue;
    private int thirdValue;
    private String teamName;
    private List<UUID> previousOpponentsList;
    private List<RoundLocationInfo> previousLocationList;

    public RoundPlayerInfo(String name, String faction) {
        this(name, faction, 0);
    }

    public RoundPlayerInfo(String name, String faction, int firstValue) {
        this(name, faction, firstValue, null);
    }

    public RoundPlayerInfo(String name, String faction, int firstValue, UUID id) {
        this.name = name;
        this.faction = faction;
        this.firstValue = firstValue;
        this.id = id != null ? id : UUID.randomUUID();
        this.secondValue = 0;
        this.thirdValue = 0;
        this.teamName = "";
        this.previousOpponentsList = new ArrayList();
        this.previousLocationList = new ArrayList();
    }
}
