package net.uveSoftware.MTSystem.WebController;

import lombok.Data;

import java.util.List;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
@Data
public class DataResponse<T> {

    private List<T> data;

    private PageInfo metadata;

    public DataResponse(List<T> data, PageInfo pageInfo) {
        this.data = data;
        this.metadata = pageInfo;
    }
}
