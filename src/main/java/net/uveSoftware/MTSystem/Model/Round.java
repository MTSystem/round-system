package net.uveSoftware.MTSystem.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
@ToString(exclude="tournament")
public class Round extends Base {
    private int roundNumber;

    @OneToMany(mappedBy = "round")
    private List<RoundMatch> roundMatchList;

    @JsonIgnore
    @ManyToOne
    private Tournament tournament;

    public Round() {
        this.roundMatchList = new ArrayList<>();
        this.roundMatchList = new ArrayList<>();
    }
}
