package net.uveSoftware.MTSystem.WebController;

import net.uveSoftware.MTSystem.Model.Player;
import net.uveSoftware.MTSystem.Model.Round;
import net.uveSoftware.MTSystem.Model.Tournament;
import net.uveSoftware.MTSystem.Model.UserProfile;
import net.uveSoftware.MTSystem.Repository.PlayerRepository;
import net.uveSoftware.MTSystem.Repository.TournamentRepository;
import net.uveSoftware.MTSystem.Repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
@RestController
@RequestMapping("entities/v1/tournaments")
public class TournamentController {

    @Autowired private TournamentRepository repository;

    @Autowired private PlayerRepository playerRepository;

    @Autowired private UserProfileRepository userProfileRepository;

    public TournamentController() {

    }
//
//    @RequestMapping(method = RequestMethod.GET)
//    public DataResponse<Tournament> get() {
//        List<Tournament> data = repository.findAll();
//        DataResponse<Tournament> response = new DataResponse<>(data, null);
//        return response;
//    }

    @Transactional
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Tournament get(@PathVariable(value = "id") UUID id) {

        Tournament tournament = this.repository.findOne(id);
        return tournament;
    }

    @Transactional
    @RequestMapping(value = "{id}/create-round/{round-id}", method = RequestMethod.GET)
    public String createRound(@PathVariable(value = "id") UUID id) {

        Tournament tournament = this.repository.findOne(id);
        return "Mola!";
    }

    @RequestMapping(method = RequestMethod.GET)
    public DataResponse<Tournament> getPageable(@RequestParam(value="limit", defaultValue="10") int limit,
                                                @RequestParam(value="offset", defaultValue="0") int offset) {
        Pageable pageable = new PageRequest(offset, limit);
        Page<Tournament> pageResult = this.repository.findAll(pageable);
        List<Tournament> data = pageResult.getContent();
        DataResponse<Tournament> response = new DataResponse<>(data, new PageInfo(limit, offset, (int)pageResult.getTotalElements()));
        return response;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    public CreateResponse createTournament(@RequestBody TournamentResource tSource) {

        try {
            Tournament tournament = this.buildTournament(tSource);
            Tournament savedTournament = this.repository.save(tournament);
            CreateResponse response = new CreateResponse(savedTournament.getId());
            return response;
        } catch (DataIntegrityViolationException exception) {
            exception.printStackTrace();
            throw exception;
        }
    }

    private Tournament buildTournament(TournamentResource tSource) {
        List<EntityError> errors = new ArrayList<>();
        EntityError entityError = null;
        Tournament tournament = tSource.getTournament();

        if (tournament.getNumberOfRounds() != tournament.getRoundList().size()) {
            tournament.getRoundList().clear();

            for (int i = 1; i <= tournament.getNumberOfRounds(); i++) {
                Round r = new Round();
                r.setTournament(tournament);
                r.setRoundNumber(i);
                tournament.getRoundList().add(r);
            }
        }

        if (tournament.getCreationDate() == null) {
            tournament.setCreationDate(new Date());
        }

        if (tournament.getNumberOfPlayers() <= 0 && tSource.getSubscriberIdList().isEmpty()) {
            entityError = new EntityError(tournament.getClass().getName(), "numberOfPlayers", "Number of players is required");
            errors.add(entityError);
        } else if (tSource.getSubscriberIdList().size() > 0) {
            tournament.setNumberOfPlayers(tSource.getSubscriberIdList().size());
        }

        if (tSource.getSubscriberIdList().size() > 0) {

            List<Player> players = this.playerRepository.findAll(tSource.getSubscriberIdList());
            tSource.getSubscriberIdList().clear();

            for (Player player: players) {
                player.setTournament(tournament);
                tournament.getSubscriberList().add(player);
            }
        }

        if (tSource.getOwnerIdList().size() > 0) {

            List<UserProfile> users = this.userProfileRepository.findAll(tSource.getOwnerIdList());
            tournament.getOwnerList().clear();

            for (UserProfile user: users) {
                user.getOnwerTournamentList().add(tournament);
                tournament.getOwnerList().add(user);
            }
        }
        else {
            entityError = new EntityError(tournament.getClass().getName(), "ownerList", "No tournament owners");
            errors.add(entityError);
        }

        if (!errors.isEmpty()) {
            throw new InvalidTournamentException("Invalid tournament", errors);
        }

        return tournament;
    }

}
