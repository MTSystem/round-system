package net.uveSoftware.MTSystem.WebController;

import net.uveSoftware.MTSystem.Model.Round;
import net.uveSoftware.MTSystem.Model.UserProfile;
import net.uveSoftware.MTSystem.Repository.RoundRepository;
import net.uveSoftware.MTSystem.Repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
@RestController
@RequestMapping("/entities/v1/rounds")
public class RoundController {

    @Autowired private RoundRepository userProfileRepository;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Round get(@PathVariable(value = "id") UUID id) {

        return this.userProfileRepository.findOne(id);
    }
}