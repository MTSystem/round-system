package net.uveSoftware.MTSystem.Test;

import net.uveSoftware.MTSystem.RoundSystem.Constrains.DistintOpponentConstrain;
import net.uveSoftware.MTSystem.RoundSystem.Constrains.RoundConstrain;
import net.uveSoftware.MTSystem.RoundSystem.RoundLocationInfo;
import net.uveSoftware.MTSystem.RoundSystem.RoundMatchInfo;
import net.uveSoftware.MTSystem.RoundSystem.RoundPlayerInfo;
import net.uveSoftware.MTSystem.RoundSystem.RoundSystemEngine;
import net.uveSoftware.TestTools.ReflectionHelper;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
public class MatcherEngineTest {

    @Test
    public void sortPlayerTest() {
        RoundPlayerInfo p1 = new RoundPlayerInfo("Estetioeslaostia", "rotohaa", 0);
        RoundPlayerInfo p2 = new RoundPlayerInfo("Vic", "haqqi", 0);

        RoundSystemEngine engine = new RoundSystemEngine(new ArrayList<RoundPlayerInfo>(),
                new ArrayList<RoundLocationInfo>(), new ArrayList<RoundConstrain>());
        Class[] c = new Class[2];
        c[0] = RoundPlayerInfo.class;
        c[1] = RoundPlayerInfo.class;

        Method m = ReflectionHelper.getPrivateMethod(engine, "sortPlayers", c);

        Assert.assertNotNull(m);

        m.setAccessible(true);
        int r = (int) ReflectionHelper.executeMethod(m, engine, p1, p2);
        Assert.assertEquals(0, r);

        p1.setThirdValue(200);
        p2.setThirdValue(100);
        r = (int) ReflectionHelper.executeMethod(m, engine, p1, p2);
        Assert.assertEquals(1, r);

        p1.setSecondValue(100);
        p2.setSecondValue(200);
        r = (int) ReflectionHelper.executeMethod(m, engine, p1, p2);
        Assert.assertEquals(-1, r);

        p1.setFirstValue(200);
        p2.setFirstValue(100);
        r = (int) ReflectionHelper.executeMethod(m, engine, p1, p2);
        Assert.assertEquals(1, r);
    }

    @Test
    public void findLocationTest() {
        RoundPlayerInfo p1 = new RoundPlayerInfo("Estetioeslaostia", "rotohaa", 0);
        RoundPlayerInfo p2 = new RoundPlayerInfo("Vic", "haqqi", 0);

        RoundLocationInfo l1 = new RoundLocationInfo("1");
        RoundLocationInfo l2 = new RoundLocationInfo("2");
        RoundLocationInfo l3 = new RoundLocationInfo("3");
        RoundLocationInfo l4 = new RoundLocationInfo("4");
        RoundLocationInfo l5 = new RoundLocationInfo("5");

        List<RoundLocationInfo> locations = new ArrayList<>();
        locations.add(l1);
        locations.add(l2);
        locations.add(l3);
        locations.add(l4);
        locations.add(l5);

        RoundSystemEngine engine = new RoundSystemEngine(new ArrayList<RoundPlayerInfo>(), locations,
                new ArrayList<RoundConstrain>());
        Class[] c = new Class[3];
        c[0] = RoundPlayerInfo.class;
        c[1] = RoundPlayerInfo.class;
        c[2] = List.class;

        Method m = ReflectionHelper.getPrivateMethod(engine, "findLocation", c);
        Assert.assertNotNull(m);

        RoundLocationInfo r = (RoundLocationInfo) ReflectionHelper.executeMethod(m, engine, p1, p2, locations);
        Assert.assertEquals("1", r.getId());

        p1.getPreviousLocationList().add(l1);
        p2.getPreviousLocationList().add(l2);
        r = (RoundLocationInfo) ReflectionHelper.executeMethod(m, engine, p1, p2, locations);
        Assert.assertEquals("3", r.getId());

        p1.getPreviousLocationList().add(l3);
        p2.getPreviousLocationList().add(l3);
        r = (RoundLocationInfo) ReflectionHelper.executeMethod(m, engine, p1, p2, locations);
        Assert.assertEquals("4", r.getId());

        p1.getPreviousLocationList().add(l4);
        p2.getPreviousLocationList().add(l4);
        p1.getPreviousLocationList().add(l5);
        p2.getPreviousLocationList().add(l5);
        r = (RoundLocationInfo) ReflectionHelper.executeMethod(m, engine, p1, p2, locations);
        Assert.assertEquals(null, r);
    }

    @Test
    public void findMatchBasicTest() {
        RoundPlayerInfo p1 = new RoundPlayerInfo("Estetioeslaostia", "rotohaa", 5);
        RoundPlayerInfo p2 = new RoundPlayerInfo("Vic", "haqqi", 2);
        RoundPlayerInfo p3 = new RoundPlayerInfo("Dario", "chinos", 3);
        RoundPlayerInfo p4 = new RoundPlayerInfo("Fenix", "nomadas", 4);

        RoundLocationInfo l1 = new RoundLocationInfo("1");
        RoundLocationInfo l2 = new RoundLocationInfo("2");
        RoundLocationInfo l3 = new RoundLocationInfo("3");
        RoundLocationInfo l4 = new RoundLocationInfo("4");
        RoundLocationInfo l5 = new RoundLocationInfo("5");

        List<RoundLocationInfo> locations = new ArrayList<>();
        locations.add(l1);
        locations.add(l2);
        locations.add(l3);
        locations.add(l4);
        locations.add(l5);

        RoundSystemEngine engine = new RoundSystemEngine(new ArrayList<RoundPlayerInfo>(), locations,
                new ArrayList<RoundConstrain>());

        List<RoundPlayerInfo> players = new ArrayList<>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        Method m = ReflectionHelper.getPrivateMethod(engine, "findMatches", List.class, List.class);
        Assert.assertNotNull(m);

        List<RoundMatchInfo> matchList = (List<RoundMatchInfo>)ReflectionHelper.executeMethod(m, engine, players, locations);
        Assert.assertEquals(0, players.size());
        Assert.assertEquals(2, matchList.size());

        Assert.assertEquals(p1, matchList.get(0).getPlayerOne());
        Assert.assertEquals(p2, matchList.get(0).getPlayerTwo());
        Assert.assertEquals(l1, matchList.get(0).getLocation());
        Assert.assertEquals(p3, matchList.get(1).getPlayerOne());
        Assert.assertEquals(p4, matchList.get(1).getPlayerTwo());
        Assert.assertEquals(l2, matchList.get(1).getLocation());
    }

    @Test
    public void findMatchTest2() {
        RoundPlayerInfo p1 = new RoundPlayerInfo("Estetioeslaostia", "rotohaa", 5);
        RoundPlayerInfo p2 = new RoundPlayerInfo("Vic", "haqqi", 2);
        RoundPlayerInfo p3 = new RoundPlayerInfo("Dario", "chinos", 3);
        RoundPlayerInfo p4 = new RoundPlayerInfo("Fenix", "nomadas", 4);

        RoundLocationInfo l1 = new RoundLocationInfo("1");
        RoundLocationInfo l2 = new RoundLocationInfo("2");
        RoundLocationInfo l3 = new RoundLocationInfo("3");
        RoundLocationInfo l4 = new RoundLocationInfo("4");
        RoundLocationInfo l5 = new RoundLocationInfo("5");

        List<RoundLocationInfo> locations = new ArrayList<>();
        locations.add(l1);
        locations.add(l2);
        locations.add(l3);
        locations.add(l4);
        locations.add(l5);

        RoundSystemEngine engine = new RoundSystemEngine(new ArrayList<RoundPlayerInfo>(), locations,
                new ArrayList<RoundConstrain>());

        List<RoundPlayerInfo> players = new ArrayList<>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        //Simula que previamente han jugando una ronda.
        p1.getPreviousLocationList().add(l1);
        p2.getPreviousLocationList().add(l1);
        p1.getPreviousOpponentsList().add(p2.getId());
        p2.getPreviousOpponentsList().add(p1.getId());

        p3.getPreviousLocationList().add(l2);
        p4.getPreviousLocationList().add(l2);
        p3.getPreviousOpponentsList().add(p4.getId());
        p4.getPreviousOpponentsList().add(p3.getId());

        Method m = ReflectionHelper.getPrivateMethod(engine, "findMatches", List.class, List.class);
        Assert.assertNotNull(m);

        List<RoundMatchInfo> matchList = (List<RoundMatchInfo>)ReflectionHelper.executeMethod(m, engine, players, locations);
        Assert.assertEquals(0, players.size());
        Assert.assertEquals(2, matchList.size());

        //vuelven a jugar los mismos porque no está activada la condición de que no puedan repetir entre ellos
        Assert.assertEquals(p1, matchList.get(0).getPlayerOne());
        Assert.assertEquals(p2, matchList.get(0).getPlayerTwo());
        Assert.assertEquals(l2, matchList.get(0).getLocation());
        Assert.assertEquals(p3, matchList.get(1).getPlayerOne());
        Assert.assertEquals(p4, matchList.get(1).getPlayerTwo());
        Assert.assertEquals(l1, matchList.get(1).getLocation());
    }

    @Test
    public void findMatchWithConstrainsTest1() {
        RoundPlayerInfo p1 = new RoundPlayerInfo("Estetioeslaostia", "rotohaa", 5);
        RoundPlayerInfo p2 = new RoundPlayerInfo("Vic", "haqqi", 2);
        RoundPlayerInfo p3 = new RoundPlayerInfo("Dario", "chinos", 3);
        RoundPlayerInfo p4 = new RoundPlayerInfo("Fenix", "nomadas", 4);

        RoundLocationInfo l1 = new RoundLocationInfo("1");
        RoundLocationInfo l2 = new RoundLocationInfo("2");
        RoundLocationInfo l3 = new RoundLocationInfo("3");
        RoundLocationInfo l4 = new RoundLocationInfo("4");
        RoundLocationInfo l5 = new RoundLocationInfo("5");

        List<RoundLocationInfo> locations = new ArrayList<>();
        locations.add(l1);
        locations.add(l2);
        locations.add(l3);
        locations.add(l4);
        locations.add(l5);

        List<RoundConstrain> constrains = new ArrayList<>();
        constrains.add(new DistintOpponentConstrain());

        RoundSystemEngine engine = new RoundSystemEngine(new ArrayList<RoundPlayerInfo>(), locations, constrains);

        List<RoundPlayerInfo> players = new ArrayList<>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        //Simula que previamente han jugando una ronda.
        p1.getPreviousLocationList().add(l1);
        p2.getPreviousLocationList().add(l1);
        p1.getPreviousOpponentsList().add(p2.getId());
        p2.getPreviousOpponentsList().add(p1.getId());

        p3.getPreviousLocationList().add(l2);
        p4.getPreviousLocationList().add(l2);
        p3.getPreviousOpponentsList().add(p4.getId());
        p4.getPreviousOpponentsList().add(p3.getId());

        Method m = ReflectionHelper.getPrivateMethod(engine, "findMatches", List.class, List.class);
        Assert.assertNotNull(m);

        List<RoundMatchInfo> matchList = (List<RoundMatchInfo>)ReflectionHelper.executeMethod(m, engine, players, locations);
        Assert.assertEquals(0, players.size());
        Assert.assertEquals(2, matchList.size());

        //vuelven a jugar los mismos porque no está activada la condición de que no puedan repetir entre ellos
        Assert.assertEquals(p1, matchList.get(0).getPlayerOne());
        Assert.assertEquals(p3, matchList.get(0).getPlayerTwo());
        Assert.assertEquals(l3, matchList.get(0).getLocation());
        Assert.assertEquals(p2, matchList.get(1).getPlayerOne());
        Assert.assertEquals(p4, matchList.get(1).getPlayerTwo());
        Assert.assertEquals(l4, matchList.get(1).getLocation());
    }

    @Test
    public void findMatchWithConstrainsTest2() {
        RoundPlayerInfo p1 = new RoundPlayerInfo("Estetioeslaostia", "rotohaa", 5);
        RoundPlayerInfo p2 = new RoundPlayerInfo("Vic", "haqqi", 2);
        RoundPlayerInfo p3 = new RoundPlayerInfo("Dario", "chinos", 3);
        RoundPlayerInfo p4 = new RoundPlayerInfo("Fenix", "nomadas", 4);

        RoundLocationInfo l1 = new RoundLocationInfo("1");
        RoundLocationInfo l2 = new RoundLocationInfo("2");
        RoundLocationInfo l3 = new RoundLocationInfo("3");
        RoundLocationInfo l4 = new RoundLocationInfo("4");
        RoundLocationInfo l5 = new RoundLocationInfo("5");

        List<RoundLocationInfo> locations = new ArrayList<>();
        locations.add(l1);
        locations.add(l2);
        locations.add(l3);

        List<RoundConstrain> constrains = new ArrayList<>();
        constrains.add(new DistintOpponentConstrain());

        RoundSystemEngine engine = new RoundSystemEngine(new ArrayList<RoundPlayerInfo>(), locations, constrains);

        List<RoundPlayerInfo> players = new ArrayList<>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        //Simula que previamente han jugando una ronda.
        p1.getPreviousLocationList().add(l1);
        p2.getPreviousLocationList().add(l1);
        p1.getPreviousOpponentsList().add(p2.getId());
        p2.getPreviousOpponentsList().add(p1.getId());

        p3.getPreviousLocationList().add(l2);
        p4.getPreviousLocationList().add(l2);
        p3.getPreviousOpponentsList().add(p4.getId());
        p4.getPreviousOpponentsList().add(p3.getId());

        Method m = ReflectionHelper.getPrivateMethod(engine, "findMatches", List.class, List.class);
        Assert.assertNotNull(m);

        List<RoundMatchInfo> matchList = (List<RoundMatchInfo>)ReflectionHelper.executeMethod(m, engine, players, locations);
        Assert.assertEquals(0, players.size());
        Assert.assertEquals(2, matchList.size());

        //vuelven a jugar los mismos porque no está activada la condición de que no puedan repetir entre ellos
        Assert.assertEquals(p1, matchList.get(0).getPlayerOne());
        Assert.assertEquals(p3, matchList.get(0).getPlayerTwo());
        Assert.assertEquals(l3, matchList.get(0).getLocation());
        Assert.assertEquals(p2, matchList.get(1).getPlayerOne());
        Assert.assertEquals(p4, matchList.get(1).getPlayerTwo());
        Assert.assertEquals(l1, matchList.get(1).getLocation());
    }
}
