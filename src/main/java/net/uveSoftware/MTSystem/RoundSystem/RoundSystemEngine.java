package net.uveSoftware.MTSystem.RoundSystem;

import net.uveSoftware.MTSystem.RoundSystem.Constrains.RoundConstrain;

import java.util.ArrayList;
import java.util.List;

/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
public class RoundSystemEngine {

    private List<RoundPlayerInfo> playerInfoList;
    private List<RoundLocationInfo> locations;
    private List<RoundConstrain> constrainList;

    public RoundSystemEngine(List<RoundPlayerInfo> playerInfoList,
                             List<RoundLocationInfo> locations,
                             List<RoundConstrain> constrainList) {

        this.playerInfoList = playerInfoList;
        this.locations = locations;
        this.constrainList = constrainList;

        this.checkParams();
    }

    public RoundInfo createRound() {
        return new RoundInfo();
    }

    private void checkParams() {

    }

    private int sortPlayers(RoundPlayerInfo p1, RoundPlayerInfo p2) {
        int r = Integer.compare(p1.getFirstValue(), p2.getFirstValue());
        if (r != 0) return r;
        r = Integer.compare(p1.getSecondValue(), p2.getSecondValue());
        if (r != 0) return r;
        return Integer.compare(p1.getThirdValue(), p2.getThirdValue());
    }

    private RoundLocationInfo findLocation(RoundPlayerInfo p1, RoundPlayerInfo p2, List<RoundLocationInfo> locations) {

        for(RoundLocationInfo l : locations) {
            if (!p1.getPreviousLocationList().contains(l) && !p2.getPreviousLocationList().contains(l)) return l;
        }

        return null;
    }

    private boolean checkConstrains(RoundPlayerInfo p1, RoundPlayerInfo p2) {

        for (RoundConstrain c : this.constrainList) {
            if (!c.check(p1, p2)) return false;
        }

        return true;
    }

    private List<RoundMatchInfo> findMatches(List<RoundPlayerInfo> playerList, List<RoundLocationInfo> locations) {

        List<RoundMatchInfo> matches = new ArrayList<>();
        int index = 1;

        while(!playerList.isEmpty() && playerList.size() > index) {
            RoundPlayerInfo player = playerList.get(0);
            RoundPlayerInfo candidatePlayer = playerList.get(index);

            boolean isValidMatch = this.checkConstrains(player, candidatePlayer);

            if (isValidMatch) {
                RoundLocationInfo location = this.findLocation(player, candidatePlayer, locations);
                playerList.remove(player);
                playerList.remove(candidatePlayer);
                RoundMatchInfo match = new RoundMatchInfo(player, candidatePlayer, location);
                locations.remove(location);
                matches.add(match);
                index = 1;
            }
            else {
                index++;
            }
        }

        if (playerList.size() == 0) {

            for(RoundMatchInfo match: matches) {
                if (match.getLocation() == null) {
                    match.setLocation(locations.remove(0));
                }
            }

            return matches;
        }
        else {
            throw new RuntimeException("No es posible hacer ronda");
        }
    }
}
