package net.uveSoftware.MTSystem.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
public class Player extends Base {

    @ManyToOne
    private UserProfile user;

    @JsonIgnore
    @ManyToOne
    private Tournament tournament;

    private String faction;
}
