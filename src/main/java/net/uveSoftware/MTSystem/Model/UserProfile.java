package net.uveSoftware.MTSystem.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
public class UserProfile extends Base {

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Tournament> onwerTournamentList;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Player> subscribedTournamentList;

    @OneToOne
    private Account user;

    public UserProfile() {
        this.onwerTournamentList = new ArrayList<>();
        this.subscribedTournamentList = new ArrayList<>();
    }
}
