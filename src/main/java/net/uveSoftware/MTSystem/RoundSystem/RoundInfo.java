package net.uveSoftware.MTSystem.RoundSystem;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
public class RoundInfo {

    private int roundNumber;
    private RoundInfo previousRound;
    private List<RoundMatchInfo> currentMatches;

    public RoundInfo() {
        this.currentMatches = new ArrayList<>();
        this.roundNumber = 1;
        this.previousRound = null;
    }
}
