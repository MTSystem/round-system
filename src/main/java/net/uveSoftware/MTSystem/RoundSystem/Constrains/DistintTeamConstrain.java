package net.uveSoftware.MTSystem.RoundSystem.Constrains;

import net.uveSoftware.MTSystem.RoundSystem.RoundPlayerInfo;

/**
 * Created by vgomezg on 03/01/2017.
 */
public class DistintTeamConstrain implements RoundConstrain {

    @Override
    public Boolean check(RoundPlayerInfo p1, RoundPlayerInfo p2) {
        if (p1.getTeamName() == "" || p2.getTeamName() == "") return true;
        else if (p1.getTeamName() == p1.getTeamName()) return false;
        else return true;
    }
}
