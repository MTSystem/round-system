package net.uveSoftware.MTSystem.WebController;

import java.util.List;
import java.util.UUID;

import lombok.Data;
import net.uveSoftware.MTSystem.Model.Account;
import net.uveSoftware.MTSystem.Model.UserProfile;
import net.uveSoftware.MTSystem.Repository.AccountRepository;
import net.uveSoftware.MTSystem.Repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
@RestController
@RequestMapping("/entities/v1/users")
public class AccountController {

    @Autowired private AccountRepository userRepository;
    @Autowired private UserProfileRepository userProfileRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<AccountResource> get(@RequestParam(value="limit", defaultValue="10") int limit,
                                     @RequestParam(value="offset", defaultValue="0") int offset) {

        Pageable pageable = new PageRequest(offset, limit);
        Page<Account> userPage = this.userRepository.findAll(pageable);
        Page<AccountResource> data = userPage.map((usr) -> { return new AccountResource(usr);});
        return data.getContent();
    }

    @RequestMapping(method = RequestMethod.POST)
    public CreateResponse createUser(@RequestBody Account user) {

        try {
            Account savedAccount = this.userRepository.save(user);
            UserProfile userProfile = new UserProfile();
            userProfile.setUser(savedAccount);
            this.userProfileRepository.save(userProfile);

            return new CreateResponse(savedAccount.getId());
        } catch (DataIntegrityViolationException exception) {
            exception.printStackTrace();
            throw exception;
        }
    }

    @Data
    private class AccountResource {
        private UUID id;
        private String alias;
        private String email;

        public AccountResource(Account user) {

            this.setId(user.getId());
            this.setAlias(user.getAlias());
            this.setEmail(user.getEmail());
        }

        public AccountResource() {

        }
    }
}