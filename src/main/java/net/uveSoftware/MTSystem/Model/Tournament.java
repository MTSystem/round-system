package net.uveSoftware.MTSystem.Model;

import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.Null;
import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
public class Tournament extends Base {

    @OneToMany(mappedBy="tournament", cascade = CascadeType.ALL) private List<Round> roundList;
    @OneToMany(mappedBy="tournament") private List<Player> playerList;
    @ManyToMany private List<UserProfile> ownerList;
    @OneToMany(mappedBy="tournament") private List<Player> subscriberList;
    private Date creationDate;

    @Lob private String description;
    private String name;
    private int numberOfPlayers;
    private int numberOfRounds;
    private Date plannedDate;
    private int pointsLimit;

    public int getNumberOfLocations() {
        return this.numberOfPlayers / 2;
    }

    public Tournament() {
        this.roundList = new ArrayList<>();
        this.playerList = new ArrayList<>();
        this.ownerList = new ArrayList<>();
        this.subscriberList = new ArrayList<>();
    }
}
