package net.uveSoftware.MTSystem.Repository;

import net.uveSoftware.MTSystem.Model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
public interface AccountRepository extends JpaRepository<Account, UUID> {
}
