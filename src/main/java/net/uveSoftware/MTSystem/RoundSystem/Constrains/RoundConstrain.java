package net.uveSoftware.MTSystem.RoundSystem.Constrains;

import net.uveSoftware.MTSystem.RoundSystem.RoundPlayerInfo;

/**
 * Created by vgomezg on 03/01/2017.
 */
public interface RoundConstrain {
    Boolean check(RoundPlayerInfo item1, RoundPlayerInfo item2);
}