package net.uveSoftware.MTSystem.WebController;

import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
public class CreateResponse {

    public UUID objectId;

    public CreateResponse(UUID id) {
        this.objectId = id;
    }

}
