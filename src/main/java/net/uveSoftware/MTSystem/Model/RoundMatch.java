package net.uveSoftware.MTSystem.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
public class RoundMatch extends Base {
    @OneToOne
    private Player playerOne;

    @OneToOne
    private Player playerTwo;

    @OneToOne(mappedBy = "roundMatch")
    private RoundResult roundResult;

    @ManyToOne
    private Round round;
}
