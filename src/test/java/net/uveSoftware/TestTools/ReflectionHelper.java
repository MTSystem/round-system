package net.uveSoftware.TestTools;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
public class ReflectionHelper {
    public static Method getPrivateMethod(Object o, String methodName, Class<?>... args) {
        Method m = null;
        try {
            m = o.getClass().getDeclaredMethod(methodName, args);
            m.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return m;
    }

    public static Object executeMethod(Method m, Object o, Object... args){

        try {
            return m.invoke(o, args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void setValueToField(Object o, String propertyName, Object value) {
        try {
            Field field = o.getClass().getDeclaredField(propertyName);
            field.setAccessible(true);
            field.set(o, value);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static Object getFieldValue(Object o, String fieldName) {
        Field field = null;
        try {
            field = o.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(o);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
