package net.uveSoftware.MTSystem.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Base {

    @Id
    private UUID id;

    public Base() {
        this.id = UUID.randomUUID();
    }
}
