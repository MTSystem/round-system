package net.uveSoftware.MTSystem.RoundSystem;

import lombok.Data;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
public class RoundMatchInfo {

    private RoundPlayerInfo playerOne = null;
    private RoundPlayerInfo playerTwo = null;
    private RoundLocationInfo location = null;

    public RoundMatchInfo(RoundPlayerInfo item1, RoundPlayerInfo item2, RoundLocationInfo location) {
        this.playerOne = item1;
        this.playerTwo = item2;
        this.location = location;
    }
}