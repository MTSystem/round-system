package net.uveSoftware.MTSystem.WebController;

import lombok.Data;
import net.uveSoftware.MTSystem.Model.Account;
import net.uveSoftware.MTSystem.Model.UserProfile;
import net.uveSoftware.MTSystem.Repository.AccountRepository;
import net.uveSoftware.MTSystem.Repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
@RestController
@RequestMapping("/entities/v1/user-profiles")
public class UserProfileController {

    @Autowired private UserProfileRepository userProfileRepository;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public UserProfile get(@PathVariable(value = "id") UUID id) {

        return this.userProfileRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public DataResponse<UserProfile> get(@RequestParam(value="limit", defaultValue="10") int limit,
                                    @RequestParam(value="offset", defaultValue="0") int offset) {

        Pageable pageable = new PageRequest(offset, limit);
        Page<UserProfile> userPage = this.userProfileRepository.findAll(pageable);
        DataResponse<UserProfile> response = new DataResponse<>(userPage.getContent(), new PageInfo(limit, offset,
                (int)userPage.getTotalElements()));
        return response;
    }

}