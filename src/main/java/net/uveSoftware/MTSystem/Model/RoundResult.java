package net.uveSoftware.MTSystem.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Master Tournament system
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
public class RoundResult extends Base {

    @OneToOne
    private RoundMatch roundMatch;

    private int PlayerOneGameResult1 = 0;
    private int PlayerOneGameResult2 = 0;
    private int PlayerOneGameResult3 = 0;

    private int PlayerTwoGameResult1 = 0;
    private int PlayerTwoGameResult2 = 0;
    private int PlayerTwoGameResult3 = 0;
}