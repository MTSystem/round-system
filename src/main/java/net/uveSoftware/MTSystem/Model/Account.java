package net.uveSoftware.MTSystem.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Created by vgomezg on 03/01/2017.
 */
@Data
@Entity
public class Account extends Base {

    @NotNull
    private String alias;
    @JsonIgnore
    @NotNull
    private String password;
    @NotNull
    private String email;

    public Account(String alias, String password) {
        this.alias = alias;
        this.password = password;
    }

    Account() { }
}
