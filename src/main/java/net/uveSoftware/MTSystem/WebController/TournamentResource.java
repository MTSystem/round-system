package net.uveSoftware.MTSystem.WebController;

import lombok.Data;
import net.uveSoftware.MTSystem.Model.Tournament;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
@Data
public class TournamentResource {

    private List<UUID> ownerIdList;
    private List<UUID> subscriberIdList;

    private Date creationDate;
    private String description;
    private String name;
    private int numberOfPlayers;
    private int numberOfRounds;
    private Date plannedDate;
    private int pointsLimit;

    public TournamentResource() {
        this.ownerIdList = new ArrayList<>();
        this.subscriberIdList = new ArrayList<>();
    }

    public TournamentResource(Tournament tournament) {
        this();

        this.creationDate = tournament.getCreationDate();
        this.description = tournament.getDescription();
        this.name = tournament.getName();
        this.numberOfPlayers = tournament.getNumberOfPlayers();
        this.numberOfRounds = tournament.getNumberOfRounds();
        this.plannedDate = tournament.getPlannedDate();
        this.pointsLimit = tournament.getPointsLimit();

        tournament.getOwnerList().forEach(userProfile -> this.ownerIdList.add(userProfile.getId()));
        tournament.getSubscriberList().forEach(player -> this.subscriberIdList.add(player.getId()));
    }

    public Tournament getTournament() {
        Tournament tournament = new Tournament();
        tournament.setCreationDate(this.getCreationDate());
        tournament.setNumberOfPlayers(this.getNumberOfPlayers());
        tournament.setDescription(this.getDescription());
        tournament.setName(this.getName());
        tournament.setNumberOfRounds(this.numberOfRounds);
        tournament.setPlannedDate(this.getPlannedDate());
        tournament.setPointsLimit(this.getPointsLimit());

        return tournament;
    }
}
