package net.uveSoftware.MTSystem.Repository;

import net.uveSoftware.MTSystem.Model.Account;
import net.uveSoftware.MTSystem.Model.UserProfile;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

/**
 * Master Tournament system
 * Created by vgomezg on 04/01/2017.
 */
public interface UserProfileRepository extends JpaRepository<UserProfile, UUID> {

}
