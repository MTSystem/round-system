package net.uveSoftware.MTSystem.WebController;

import java.util.List;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
public class InvalidTournamentException extends RuntimeException {

    private String message;
    private List<EntityError> errors;

    public InvalidTournamentException(String message, List<EntityError> errors) {
        this.message = message;
        this.errors = errors;
    }
}
