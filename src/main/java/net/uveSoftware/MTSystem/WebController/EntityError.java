package net.uveSoftware.MTSystem.WebController;

import lombok.Data;

/**
 * Master Tournament system
 * Created by vgomezg on 05/01/2017.
 */
@Data
public class EntityError {

    private String entityClass;
    private String field;
    private String message;

    public EntityError(String entityClass, String field, String message) {
        this.entityClass = entityClass;
        this.field = field;
        this.message = message;
    }
}
