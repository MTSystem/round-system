package net.uveSoftware.MTSystem.RoundSystem.Constrains;

import net.uveSoftware.MTSystem.RoundSystem.RoundPlayerInfo;

/**
 * Created by vgomezg on 03/01/2017.
 */
public class DistintOpponentConstrain implements RoundConstrain {

    @Override
    public Boolean check(RoundPlayerInfo item1, RoundPlayerInfo item2) {
        if (item1.getPreviousOpponentsList().contains(item2.getId())
                || item2.getPreviousOpponentsList().contains(item1.getId())) {
            return false;
        } else {
            return true;
        }
    }
}
